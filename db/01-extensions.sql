/*
 EXTENSIONS
 
 PostgreSQL module to enable UUID generation
 See https://www.postgresql.org/docs/current/uuid-ossp.html
 */
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";