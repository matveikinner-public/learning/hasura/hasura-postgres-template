# Hasura

## Installation

### Docker

#### Docker Compose

##### Environment variables

Docker will check and populate environment variables to `docker-compose.yml` file by default from `.env` file inside parent (or root) directory. To check the actual `docker-compose.yml` file output run `docker compose convert`.

To override these `.env` file values you can pass specify another file by passing `--env-file <path/to/env>`

For official documentation see [this](https://docs.docker.com/compose/environment-variables/).

### Hasura

#### Initialize database

There are primarily two ways to initialize database with extensions, databases, tables and data.

1. Using Postgres you can mount in `docker-compose.yml` a host directory to a volume destination folder `docker-entrypoint-initdb.d` which will run any .sql files in alphabetical order. For official documentation see "Initialization scripts" [section](https://hub.docker.com/_/postgres/).
2. Using Hasura metadata (assuming there are any) you can run `hasura metadata apply`. In order to obtain Hasura metadata you may need to initialize hasura and run `hasura init`, create tables and possibly insert data after which run `hasura metadata export` which will populate Hasura specific folder(s) (which Hasura creates upon init command) with metadata and migration script(s). Pay attention that Hasura might require to use flag `--admin-secret=<admin-secret-here>` to have admin right to make such actions on Hasura. For official documentation see [this](https://hasura.io/docs/latest/deployment/graphql-engine-flags/config-examples/).
